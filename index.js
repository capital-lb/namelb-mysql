
let mysql = require("mysql");
let $createConnection=require("./lib/$createConnection");
let $createPool=require("./lib/$createPool");

mysql.$createConnection = $createConnection;

mysql.$createPool = $createPool;

module.exports = mysql;


