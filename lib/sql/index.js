
function _keys(data) {
    let str = "";
    for (let i in data) {
        if (str == "") {
            str = i;
        } else {
            str += "," + i;
        }
    }
    return str;
}
function _values(data) {
    let str = "";
    for (let i in data) {
        let s = ""
        if (typeof data[i] == "string") {
            s = '"' + data[i] + '"';
        } else if (typeof data[i] == "function") {
            s = data[i]() + '()';
        }
        if (str == "") {
            str = s;
        } else {
            str += "," + s;
        }
    }
    return str;
}
function _where(data) {
    let str = "";
    for (let i in data) {
        let s = ""
        if (typeof data[i] == "string") {
            s = '"' + data[i] + '"';
        } else if (typeof data[i] == "function") {
            s = data[i]() + '()';
        }
        if (str == "") {
            str = i + "=" + s;
        } else {
            str += " and " + i + "=" + s;
        }
    }
    return str;
}
function _set(data) {
    let str = "";
    for (let i in data) {
        let s = ""
        if (typeof data[i] == "string") {
            s = '"' + data[i] + '"';
        } else if (typeof data[i] == "function") {
            s = data[i]() + '()';
        }
        if (str == "") {
            str = i + "=" + s;
        } else {
            str += " , " + i + "=" + s;
        }
    }
    return str;
}


function sql({ method, data, condition, table, sort, reverse, limit, skip }) {
    if (method == "insert" || method == "INSERT") {
        let sql = `insert into ${table} (${_keys(data)}) values (${_values(data)})`;
        return sql;
    }
    if (method == "delete" || method == "DELETE") {
        let sql = `delete from ${table}`;
        if (condition) {
            sql += " where " + _where(condition);
        }
        return sql;
    }
    if (method == "update" || method == "UPDATE") {
        let sql = `update ${table} set ${_set(data)}`;
        if (condition) {
            sql += " where " + _where(condition);
        }
        return sql;
    }
    if (method == "select" || method == "SELECT") {
        let sql = `select * from ${table} `;
        if (condition) {
            sql += " where " + _where(condition);
        }
        sql += ` ${sort ? "order by " + sort : ""} ${reverse ? " desc " : ""} ${limit ? "limit " + limit : ""} ${skip ? "offset " + skip : ""} `

        return sql;
    }
    return "";
}

module.exports = sql;