let mysql = require("mysql");
const control = require("./control");

function $createConnection(config, error) {
    let connection = mysql.createConnection(config);


    connection.insert = function ({ data, table }) {
        return new Promise((resolve, reject) => {
            control.insert({ data: data, table: table, connection: this })
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                })
        })
    };



    connection._delete = function ({ condition, table }) {
        return new Promise((resolve, reject) => {
            control._delete({ condition: condition, table: table, connection: this })
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                })
        })
    }


    
    connection.update = function ({ data, condition, table }) {
        return new Promise((resolve, reject) => {
            control.update({ data: data, condition: condition, table: table, connection: this })
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                })

        })
    };



    connection.select = function ({ condition, table, sort, limit, skip, reverse }) {
        return new Promise((resolve, reject) => {
            control.select({ condition: condition, table: table, sort: sort, limit: limit, skip: skip, reverse: reverse, connection: this })
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                })
        })
    };

    connection.count = function ({ condition, table}) {
        return new Promise((resolve, reject) => {
            control.count({ condition: condition, table: table, connection: this })
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                })
        })
    };

    connection.$query = function (sql) {
        return new Promise((resolve, reject) => {
            this.query(sql, (err, res) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(res);
                }
            })
        })
    }



    return connection;
}

module.exports = $createConnection;