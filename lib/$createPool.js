let mysql = require("mysql");
const control = require("./control");

function $createPool(config) {
    let pool = mysql.createPool(config);


    pool.insert = function ({ data, table }) {
        return new Promise((resolve, reject) => {
            this.getConnection((err, connection) => {
                if (err) {
                    reject(err);
                } else {
                    control.insert({ data: data, table: table, connection: connection })
                        .then(res => {
                            resolve(res);
                        })
                        .catch(err => {
                            reject(err);
                        })
                    connection.release();
                }

            })
        })

    };


    pool._delete = function ({ condition, table }) {
        return new Promise((resolve, reject) => {
            this.getConnection((err, connection) => {
                if (err) {
                    reject(err)
                } else {
                    control._delete({ condition: condition, table: table, connection: connection })
                        .then(res => {
                            resolve(res);
                        })
                        .catch(err => {
                            reject(err);
                        })
                    connection.release();
                }

            })
        })
    }


    pool.update = function ({ data, condition, table }) {
        return new Promise((resolve, reject) => {
            this.getConnection((err, connection) => {
                if (err) {
                    reject(err)
                } else {
                    control.update({ data: data, condition: condition, table: table, connection: connection })
                        .then(res => {
                            resolve(res);
                        })
                        .catch(err => {
                            reject(err);
                        })
                    connection.release();
                }

            })
        })
    };


    pool.select = function ({ condition, table, sort, limit, skip, reverse }) {
        return new Promise((resolve, reject) => {
            this.getConnection((err, connection) => {
                if (err) {
                    reject(err)
                } else {
                    control.select({ condition: condition, table: table, sort: sort, limit: limit, skip: skip, reverse: reverse, connection: connection })
                        .then(res => {
                            resolve(res);
                        })
                        .catch(err => {
                            reject(err);
                        })
                    connection.release();
                }

            })

        })
    };


    pool.count = function ({ condition, table, sort, limit, skip, reverse }) {
        return new Promise((resolve, reject) => {
            this.getConnection((err, connection) => {
                if (err) {
                    reject(err)
                } else {
                    control.count({ condition: condition, table: table, connection: connection })
                        .then(res => {
                            resolve(res);
                        })
                        .catch(err => {
                            reject(err);
                        })
                    connection.release();
                }

            })

        })
    };

    pool.$query = function (sql) {
        return new Promise((resolve, reject) => {
            this.getConnection((err, connection) => {
                if (err) {
                    reject(err)
                } else {
                    connection.query(sql, (err, res) => {
                        if (err) {
                            reject(err)
                        } else {
                            resolve(res);
                        }
                    })
                    connection.release();
                }

            })
        })
    }

    pool.$getConnection = $getConnection;


    return pool;
}
module.exports = $createPool;


function $getConnection(callback) {
    if (callback) {
        this.getConnection((err, connection) => {
            bond(connection);
            callback(err, connection);
        })
    } else {
        return new Promise((resolve, reject) => {
            this.getConnection((err, connection) => {
                if (err) {
                    return reject(err);
                }
                bond(connection);
                resolve(connection);
            })
        })
    }

    function bond(connection) {
        connection.insert = function ({ data, table }) {
            return new Promise((resolve, reject) => {
                control.insert({ data: data, table: table, connection: this })
                    .then(res => {
                        resolve(res);
                    })
                    .catch(err => {
                        reject(err);
                    })
            })
        };



        connection._delete = function ({ condition, table }) {
            return new Promise((resolve, reject) => {
                control._delete({ condition: condition, table: table, connection: this })
                    .then(res => {
                        resolve(res);
                    })
                    .catch(err => {
                        reject(err);
                    })
            })
        }



        connection.update = function ({ data, condition, table }) {
            return new Promise((resolve, reject) => {
                control.update({ data: data, condition: condition, table: table, connection: this })
                    .then(res => {
                        resolve(res);
                    })
                    .catch(err => {
                        reject(err);
                    })

            })
        };



        connection.select = function ({ condition, table, sort, limit, skip, reverse }) {
            return new Promise((resolve, reject) => {
                control.select({ condition: condition, table: table, sort: sort, limit: limit, skip: skip, reverse: reverse, connection: connection })
                    .then(res => {
                        resolve(res);
                    })
                    .catch(err => {
                        reject(err);
                    })
            })
        };

        connection.count = function ({ condition, table }) {
            return new Promise((resolve, reject) => {
                control.count({ condition: condition, table: table, connection: connection })
                    .then(res => {
                        resolve(res);
                    })
                    .catch(err => {
                        reject(err);
                    })
            })
        };


        connection.$query = function (sql) {
            return new Promise((resolve, reject) => {
                this.query(sql, (err, res) => {
                    if (err) {
                        reject(err)
                    } else {
                        resolve(res);
                    }
                })
            })
        }
    }

}