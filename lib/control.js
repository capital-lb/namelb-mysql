
function _keys(data) {
    let str = "";
    for (let i in data) {
        if (str == "") {
            str = i;
        } else {
            str += "," + i;
        }
    }
    return str;
}
function _values(data) {
    let str = "";
    for (let i in data) {
        let s = ""
        if (typeof data[i] == "string") {
            s = '"' + data[i] + '"';
        } else if (typeof data[i] == "function") {
            s = data[i]() + '()';
        }else{
            s=data[i];
        }
        if (str == "") {
            str = s;
        } else {
            str += "," + s;
        }
    }
    return str;
}
function _where(data) {
    let str = "";
    for (let i in data) {
        let s = ""
        if (typeof data[i] == "string") {
            s = '"' + data[i] + '"';
        } else if (typeof data[i] == "function") {
            s = data[i]() + '()';
        }else{
            s=data[i];
        }
        if (str == "") {
            str = i + "=" + s;
        } else {
            str += " and " + i + "=" + s;
        }
    }
    return str;
}
function _set(data) {
    let str = "";
    for (let i in data) {
        let s = ""
        if (typeof data[i] == "string") {
            s = '"' + data[i] + '"';
        } else if (typeof data[i] == "function") {
            s = data[i]() + '()';
        }else{
            s=data[i];
        }
        if (str == "") {
            str = i + "=" + s;
        } else {
            str += " , " + i + "=" + s;
        }
    }
    return str;
}
function insert({ data, table, connection }) {
    return new Promise((resolve, reject) => {
        let sql = `insert into ${table} (${_keys(data)}) values (${_values(data)})`;
        connection.query(sql, (err, res) => {
            if (err) {
                console.log(sql);
                reject(err);
            } else {
                resolve(res);
            }
        })
    })
}
function _delete({ condition, table, connection }) {
    return new Promise((resolve, reject) => {
        let sql = `delete from ${table}`;
        if (condition) {
            sql += " where " + _where(condition);
        }
        connection.query(sql, (err, res) => {
            if (err) {
                console.log(sql);
                reject(err);
            } else {
                resolve(res);
            }
        })
    })
}
function update({ data, condition, table, connection }) {
    return new Promise((resolve, reject) => {
        let sql = `update ${table} set ${_set(data)}`;
        if (condition) {
            sql += " where " + _where(condition);
        }
        connection.query(sql, (err, res) => {
            if (err) {
                console.log(sql);
                reject(err);
            } else {
                resolve(res);
            }
        })
    })
}
function select({ condition, table, connection, sort, limit, skip, reverse }) {
    return new Promise((resolve, reject) => {
        let sql = `select * from ${table} `;
        if (condition) {
            sql += " where " + _where(condition);
        }
        sql += ` ${sort ? "order by " + sort : ""} ${reverse ? " desc " : ""} ${limit ? "limit " + limit : ""} ${skip ? "offset " + skip : ""} `
        connection.query(sql, (err, res) => {
            if (err) {
                console.log(sql);
                reject(err);
            } else {
                resolve(res);
            }
        })
    })

}
function count({ condition, table, connection }) {
    return new Promise((resolve, reject) => {
        let sql = `select count(*) from ${table} `;
        if (condition) {
            sql += " where " + _where(condition);
        }
        connection.query(sql, (err, res) => {
            if (err) {
                console.log(sql);
                reject(err);
            } else {
                resolve(res);
            }
        })
    })

}
function deepCopy(data) {
    let obj = {};
    for (let key in data) {
        if (typeof data[key] == "object") {
            obj[key] = deepCopy(data[key]);
        }
        obj[key] = data[key];
    }
    return obj;
}

module.exports = {
    _keys, _values, _where, _set, insert, _delete, update, select, deepCopy, count
}