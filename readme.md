#### 代码的基本使用如下



```js
const mysql = require("namelb-mysql");

let connection = mysql.$createConnection({
    host: "127.0.0.1",
    user: "root",
    password: "password",
    database: "test",
    port: 3306
});

connection.insert({
    data: { id: "0001", name: "name", password: "password" },
    table: "test"
})


connection.update({
    data: {   password: "PASSWORD" },
    condition: { id: "0001" },
    table: "test"
})

connection.select({
    condition: { id: "0001" },
    table: "test"
}).then(res => {
    console.log(res);
})

connection._delete({
    condition: { id: "0001" },
    table: "test"
})

connection.query("select * from test",(err,res)=>{
    if(err){
        console.log(err);
    }else{
        console.log(res);
    }
})



let pool = mysql.$createPool({
    host: "127.0.0.1",
    user: "root",
    password: "password",
    database: "test",
    port: 3306
});

pool.select({ table: "user" }).then(res => {
    console.log(res);
})

pool.$getConnection().then(connction => {
    connction.select({ table: "user" }).then(res => {
        console.log(res);
    })
    connction.release();
})

pool.$getConnection((err, connection) => {
    connection.select({ table: "user" }).then(res => {
        console.log(res);
    })
})

pool.$getConnection((err, connection) => {
    connection.query("select * from user ", (err, res) => {
        console.log(res);
    })
})

```

### 参数列表

###### insert  ,  _delete  ,  update  ,  select
* data : 需要插入或更新的数据 , 对象
* table : 数据库表名
* condition : 更新或查询数据的条件 , 对象
* sort : 根据指定元素排序 , 字符串
* reverse : 是否倒序 , 布尔值
* limit : 返回数据数量 , 数字
* skip : 跳过数据数量 , 数字